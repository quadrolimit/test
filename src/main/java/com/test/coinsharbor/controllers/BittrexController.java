package com.test.coinsharbor.controllers;

import com.test.coinsharbor.models.StatisticDto;
import com.test.coinsharbor.services.BittrexService;
import org.knowm.xchange.bittrex.BittrexUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/bittrex")
public class BittrexController {

    private final BittrexService bittrexService;

    @Autowired
    public BittrexController(BittrexService bittrexService) {
        this.bittrexService = bittrexService;
    }

    @GetMapping("/daily-statistic/{pair}")
    public ResponseEntity<StatisticDto> getDailyStatistic(@PathVariable("pair") String currencyPair) {
        return ResponseEntity.ok(bittrexService.getDailyStatistic(BittrexUtils.toCurrencyPair(currencyPair)));
    }
}
