package com.test.coinsharbor.api;

import com.test.coinsharbor.exceptions.BittrexException;
import lombok.extern.slf4j.Slf4j;
import org.knowm.xchange.currency.CurrencyPair;
import org.knowm.xchange.dto.marketdata.OrderBook;
import org.knowm.xchange.dto.marketdata.Ticker;
import org.knowm.xchange.dto.marketdata.Trades;
import org.knowm.xchange.service.marketdata.MarketDataService;
import org.knowm.xchange.service.marketdata.params.CurrencyPairsParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.io.IOException;
import java.util.List;

@Slf4j
@Service
public class BittrexApi {

    private final MarketDataService bittrexMarketDataService;

    @Autowired
    public BittrexApi(@Lazy MarketDataService bittrexMarketDataService) {
        this.bittrexMarketDataService = bittrexMarketDataService;
    }

    public Ticker getTicker(@NotNull CurrencyPair currencyPair) {
        try {
            return bittrexMarketDataService.getTicker(currencyPair);
        } catch (IOException ex) {
            log.error("getTicker exception", ex);
            throw new BittrexException(ex);
        }
    }

    public List<Ticker> getTickers(@NotNull CurrencyPairsParam currencyPair) {
        try {
            return bittrexMarketDataService.getTickers(currencyPair);
        } catch (IOException ex) {
            log.error("getTickers exception", ex);
            throw new BittrexException(ex);
        }
    }

    public OrderBook getOrderBook(@NotNull CurrencyPair currencyPair,
                                  @Null Integer depth) {
        try {
            return depth == null
                    ? bittrexMarketDataService.getOrderBook(currencyPair)
                    : bittrexMarketDataService.getOrderBook(currencyPair, depth);
        } catch (IOException ex) {
            log.error("getOrderBook exception", ex);
            throw new BittrexException(ex);
        }
    }

    public Trades getTrades(@NotNull CurrencyPair currencyPair) {
        try {
            return bittrexMarketDataService.getTrades(currencyPair);
        } catch (IOException ex) {
            log.error("getTrades exception", ex);
            throw new BittrexException(ex);
        }
    }
}
