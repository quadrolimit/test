package com.test.coinsharbor.configuration;

import org.knowm.xchange.Exchange;
import org.knowm.xchange.ExchangeFactory;
import org.knowm.xchange.bittrex.BittrexExchange;
import org.knowm.xchange.service.marketdata.MarketDataService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

@Configuration
public class ApiConfiguration {

    @Lazy
    @Bean("bittrexMarketDataService")
    public MarketDataService getMarketDataService() {
        Exchange exchange = ExchangeFactory.INSTANCE.createExchange(BittrexExchange.class.getName());
        return exchange.getMarketDataService();
    }
}
