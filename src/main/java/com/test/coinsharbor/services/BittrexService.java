package com.test.coinsharbor.services;

import com.test.coinsharbor.api.BittrexApi;
import com.test.coinsharbor.models.StatisticDto;
import com.test.coinsharbor.utils.VolatilityUtil;
import org.knowm.xchange.currency.CurrencyPair;
import org.knowm.xchange.dto.marketdata.Ticker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BittrexService {

    private final BittrexApi bittrexApi;

    @Autowired
    public BittrexService(BittrexApi bittrexApi) {
        this.bittrexApi = bittrexApi;
    }

    /**
     * Service method to retrieve daily statistic (volatility and trade volume)
     *
     * @param currencyPair currency pair
     * @return {@link StatisticDto}
     */
    public StatisticDto getDailyStatistic(CurrencyPair currencyPair) {
        final Ticker ticker = bittrexApi.getTicker(currencyPair);

        return StatisticDto.builder()
                .volatility(VolatilityUtil.calculate(ticker.getHigh(), ticker.getLow()))
                .volume(ticker.getVolume())
                .build();
    }
}
