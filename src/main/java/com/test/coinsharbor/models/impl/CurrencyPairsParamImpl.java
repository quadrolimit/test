package com.test.coinsharbor.models.impl;

import lombok.Builder;
import lombok.Singular;
import org.knowm.xchange.currency.CurrencyPair;
import org.knowm.xchange.service.marketdata.params.CurrencyPairsParam;

import java.util.Collection;
import java.util.Set;

@Builder
public class CurrencyPairsParamImpl implements CurrencyPairsParam {

    @Singular
    private Set<CurrencyPair> currencyPairs;

    @Override
    public Collection<CurrencyPair> getCurrencyPairs() {
        return currencyPairs;
    }
}
