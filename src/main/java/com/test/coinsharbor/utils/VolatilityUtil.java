package com.test.coinsharbor.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static java.util.Objects.requireNonNull;

public final class VolatilityUtil {

    /**
     * Util method for calculate volatility value
     *
     * @param high high price
     * @param low  low price
     * @return calculated volatility value
     */
    public static BigDecimal calculate(BigDecimal high, BigDecimal low) {
        requireNonNull(high, "High price must be provided");
        requireNonNull(low, "Low price must be provided");

        final double dHigh = high.doubleValue();
        final double dLow = low.doubleValue();

        final double volatility = 100 * (dHigh / dLow - 1);
        return BigDecimal.valueOf(volatility).setScale(8, RoundingMode.HALF_UP);
    }
}
