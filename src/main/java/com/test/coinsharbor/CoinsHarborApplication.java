package com.test.coinsharbor;

import com.test.coinsharbor.configuration.ApiConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import({ApiConfiguration.class})
public class CoinsHarborApplication {

	public static void main(String[] args) {
		SpringApplication.run(CoinsHarborApplication.class, args);
	}
}
