package com.test.coinsharbor.exceptions;

public class BittrexException extends RuntimeException {

    public BittrexException(Throwable cause) {
        super(cause);
    }
}
