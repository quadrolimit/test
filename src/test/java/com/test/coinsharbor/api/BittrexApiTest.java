package com.test.coinsharbor.api;

import com.test.coinsharbor.AbstractTest;
import com.test.coinsharbor.exceptions.BittrexException;
import com.test.coinsharbor.models.impl.CurrencyPairsParamImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.knowm.xchange.currency.CurrencyPair;
import org.knowm.xchange.dto.marketdata.OrderBook;
import org.knowm.xchange.dto.marketdata.Ticker;
import org.knowm.xchange.dto.marketdata.Trades;
import org.knowm.xchange.service.marketdata.MarketDataService;
import org.knowm.xchange.service.marketdata.params.Params;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BittrexApiTest extends AbstractTest {

    private static final CurrencyPair PAIR = CurrencyPair.BTC_USDT;

    @Mock
    private MarketDataService marketDataService;

    private BittrexApi bittrexApi;

    @Before
    public void setUp() throws Exception {
        bittrexApi = new BittrexApi(marketDataService);
    }

    @Test
    public void getTicker_successTest() throws Exception {
        when(marketDataService.getTicker(any(CurrencyPair.class))).thenReturn(getTicker());

        Ticker returnedTicker = bittrexApi.getTicker(PAIR);

        verify(marketDataService, atLeastOnce()).getTicker(any(CurrencyPair.class));

        assertNotNull(returnedTicker);
    }

    @Test(expected = BittrexException.class)
    public void getTicker_failedTest() throws Exception {
        when(marketDataService.getTicker(any(CurrencyPair.class))).thenThrow(new IOException("Something happened wrong"));

        bittrexApi.getTicker(PAIR);
    }

    @Test
    public void getTickers_successTest() throws Exception {
        when(marketDataService.getTickers(any(Params.class))).thenReturn(Collections.singletonList(getTicker()));

        CurrencyPairsParamImpl params = CurrencyPairsParamImpl.builder()
                .currencyPair(PAIR)
                .build();
        List<Ticker> tickers = bittrexApi.getTickers(params);

        verify(marketDataService, atLeastOnce()).getTickers(any(Params.class));

        assertNotNull(tickers);
        assertFalse(tickers.isEmpty());
    }

    @Test(expected = BittrexException.class)
    public void getTickers_failedTest() throws Exception {
        when(marketDataService.getTickers(any(Params.class))).thenThrow(new IOException("Something happened wrong"));

        CurrencyPairsParamImpl params = CurrencyPairsParamImpl.builder()
                .currencyPair(PAIR)
                .build();
        bittrexApi.getTickers(params);
    }

    @Test
    public void getOrderBook_successTest() throws Exception {
        when(marketDataService.getOrderBook(any(CurrencyPair.class))).thenReturn(getOrderBook());

        OrderBook returnedOrderBook = bittrexApi.getOrderBook(PAIR, null);

        verify(marketDataService, atLeastOnce()).getOrderBook(any(CurrencyPair.class));

        assertNotNull(returnedOrderBook);
    }

    @Test(expected = BittrexException.class)
    public void getOrderBook_failedTest() throws Exception {
        when(marketDataService.getOrderBook(any(CurrencyPair.class))).thenThrow(new IOException("Something happened wrong"));

        bittrexApi.getOrderBook(PAIR, null);
    }

    @Test
    public void getTrades_successTest() throws Exception {
        Trades trades = mock(Trades.class);
        when(marketDataService.getTrades(any(CurrencyPair.class))).thenReturn(trades);

        Trades returnedTrades = bittrexApi.getTrades(PAIR);

        verify(marketDataService, atLeastOnce()).getTrades(any(CurrencyPair.class));

        assertNotNull(returnedTrades);
    }

    @Test(expected = BittrexException.class)
    public void getTrades_failedTest() throws Exception {
        when(marketDataService.getTrades(any(CurrencyPair.class))).thenThrow(new IOException("Something happened wrong"));

        bittrexApi.getTrades(PAIR);
    }
}
