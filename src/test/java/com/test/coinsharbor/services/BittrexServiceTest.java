package com.test.coinsharbor.services;

import com.test.coinsharbor.AbstractTest;
import com.test.coinsharbor.api.BittrexApi;
import com.test.coinsharbor.exceptions.BittrexException;
import com.test.coinsharbor.models.StatisticDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.knowm.xchange.currency.CurrencyPair;
import org.knowm.xchange.dto.marketdata.Ticker;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BittrexServiceTest extends AbstractTest {

    private static final CurrencyPair PAIR = CurrencyPair.BTC_USDT;

    @Mock
    private BittrexApi bittrexApi;

    private BittrexService bittrexService;

    @Before
    public void setUp() throws Exception {
        bittrexService = new BittrexService(bittrexApi);
    }

    @Test
    public void getDailyStatistic_successTest() throws Exception {
        when(bittrexApi.getTicker(any(CurrencyPair.class))).thenReturn(getTicker());

        StatisticDto dailyStatistic = bittrexService.getDailyStatistic(PAIR);

        verify(bittrexApi, atLeastOnce()).getTicker(any(CurrencyPair.class));

        assertNotNull(dailyStatistic);
        assertNotNull(dailyStatistic.getVolatility());
        assertNotNull(dailyStatistic.getVolume());
    }

    @Test(expected = BittrexException.class)
    public void getDailyStatistic_failedTest() throws Exception {
        when(bittrexApi.getTicker(any(CurrencyPair.class))).thenThrow(BittrexException.class);

        bittrexService.getDailyStatistic(PAIR);
    }
}
