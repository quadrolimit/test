package com.test.coinsharbor.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.coinsharbor.models.StatisticDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BittrexControllerTest {

    private static final String CURRENCY_PAIR = "USDT-BTC";

    private static final String DAILY_STATISTIC = "/bittrex/daily-statistic/{pair}";

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void getDailyStatistic_successTest() throws Exception {
        MvcResult mvcResult = mockMvc.perform(
                get(DAILY_STATISTIC, CURRENCY_PAIR))
                .andExpect(status().isOk())
                .andReturn();

        StatisticDto statisticDto = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), StatisticDto.class);

        assertNotNull(statisticDto);
        assertNotNull("Volatility couldn't be null", statisticDto.getVolatility());
        assertNotNull("Trade volume couldn't be null", statisticDto.getVolume());
    }
}
