package com.test.coinsharbor.utils;

import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static org.junit.Assert.assertEquals;

public class VolatilityUtilTest {

    @Test
    public void calculate_successTest() {
        BigDecimal high = BigDecimal.valueOf(7000);
        BigDecimal low = BigDecimal.valueOf(6500);

        BigDecimal expectedVolatility = BigDecimal.valueOf(100)
                .multiply(high.divide(low, 8, RoundingMode.HALF_UP).subtract(BigDecimal.ONE));

        BigDecimal volatility = VolatilityUtil.calculate(high, low);

        assertEquals("The difference should not exceed delta value", expectedVolatility.doubleValue(), volatility.doubleValue(), 1e-6);
    }
}
