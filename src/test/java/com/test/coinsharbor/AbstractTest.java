package com.test.coinsharbor;

import org.knowm.xchange.dto.marketdata.OrderBook;
import org.knowm.xchange.dto.marketdata.Ticker;
import org.knowm.xchange.dto.trade.LimitOrder;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Collections;
import java.util.Date;

import static org.mockito.Mockito.mock;

public abstract class AbstractTest {

    protected Ticker getTicker() throws Exception {
        return Ticker.Builder.class.newInstance()
                .high(BigDecimal.valueOf(7000))
                .low(BigDecimal.valueOf(6500))
                .volume(BigDecimal.valueOf(10000))
                .build();
    }

    protected OrderBook getOrderBook() {
        LimitOrder limitOrder = mock(LimitOrder.class);
        return new OrderBook(
                Date.from(Instant.now()),
                Collections.singletonList(limitOrder),
                Collections.singletonList(limitOrder));
    }
}
